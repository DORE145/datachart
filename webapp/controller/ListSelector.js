sap.ui.define([
	"sap/ui/base/Object"
], function(BaseObject) {
	
	return BaseObject.extend("test.dore.dataChart.controller.ListSelector", {
		
		constructor: function() {
			this._oWhenListHasBeenSet = new Promise(function(fnResolveListHasBeenSet) {
				this._fnResolveListHasBeenSet = fnResolveListHasBeenSet;
			}.bind(this));
			
			this.oWhenListLoadingIsDone = new Promise(function(fnResolve, fnReject) {
				this._oWhenListHasBeenSet
						.then(function(oList) {
						oList.getBinding("items").attachEventOnce("dataRecieved",
							function(oData) {
								if(!oData.getParameter("data")) {
									fnReject({
										list: oList,
										error: true
									});
								}
								var oListFirstItem = oList.getItems()[0];
								if (oListFirstItem) {
									//Have to make sure that first list item is selected
									//and select event is trigered
									fnResolve({
										list: oList,
										firstListItem: oListFirstItem
									});
								} else {
									//No items in list
									fnReject({
										list: oList,
										error: false
									});
								}
							});
					});
			}.bind(this));                      
		},
		
		setBoundMasterList: function(oList) {
			this._oList = oList;
			this._fnResolveListHasBeenSet(oList);
		}
		
		
	});
});