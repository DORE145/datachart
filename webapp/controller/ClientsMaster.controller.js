sap.ui.define([
	"test/dore/dataChart/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(BaseController, JSONModel, ODataModel , Filter, FilterOperator, Device) {
	"use strict";
	
	return BaseController.extend("test.dore.dataChart.controller.ClientsMaster", {
		
		
		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */
		
		onInit: function() {
			var oList = this.byId("clientsList"),
				oViewModel = this._createViewModel(),
				iOriginalBusyDelay = oList.getBusyIndicatorDelay();
				
			this._oList = oList;
			//keep filter and search state
			this._oListFilterState = {
				aFilter: [],
				aSearch: []
			};
			
			this.setModel(oViewModel, "masterView");
			// Make sure, busy indication is showing immediately so there is no
			// break after the busy indication for loading the view's meta data is ended
			oList.attachEventOnce("updateFinished", function() {
				// Restore original busy indicator delay for the list
				oViewModel.setProperty("/delay", iOriginalBusyDelay);
			});
			
			this.getView().addEventDelegate({
				onBeforeFirstShow: function() {
					this.getOwnerComponent().oListSelector.setBoundMasterList(oList);
				}.bind(this)
			});
			
			this.getRouter().getRoute("master").attachPatternMatched(this._onMasterMatched, this);
			this.getRouter().attachBypassed(this.onBypassed, this);
		},
		
		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */
		
		//Updates count list items and hides pull to refresh if nessesary
		onUpdateFinished: function(oEvent) {
			this._updateListItemCount(oEvent.getParameter("total"));
			this.byId("pullToRefresh").hide();
		},
		
		onSearch: function(oEvent) {
			if (oEvent.getParameters().refreshButtonPressed) {
				this.onRefresh();
				return;
			}
			
			var sQuery = oEvent.getSource().getValue();
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("Name", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			
			this._applyFilterSearch();
		},
		
		//Refreshes list, keeps sorting and filter settings
		onRefresh: function() {
			this._oList.getBinding("items").refresh();	
		},
		
		onSelectionChange: function(oEvent) {
			this._showDetail(oEvent.getParameter("listItem"));
		},
		
		//When no route matched
		onBypassed: function() {
			this._oList.removeSelections(true);          
		},
		
		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */
		
		_createViewModel: function() {
			return new JSONModel({
				isFilterBarVisible: false,
				filterBarLabel: "",
				delay: 0,
				title: this.getResourceBundle().getText("masterTitleCount", [0]),
				noDataText: this.getResourceBundle().getText("masterListNoDataText"),
				sortBy: "Name",
				groupBy: "None"
			});
		},
		
		_updateListItemCount: function(iTotalItems) {
			var sTitle;
			//Upldate only if length is final
			if (this._oList.getBinding("items").isLengthFinal()) {
				sTitle = this.getResourceBundle().getText("masterTitleCount", [iTotalItems]);
				this.getModel("masterView").setProperty("/title", sTitle);
			}
		},
		
		_applyFilterSearch: function() {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
			oViewModel = this.getModel("masterView");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataWithFilterOrSearchText"));
			} else if (this._oListFilterState.aFilter.length > 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataText"));
			}
		},
		
		_showDetail: function(oItem) {
			var bReplace = !Device.system.phone;
			this.getRouter().navTo("object", {
				objectId: oItem.getBindingContext().getProperty("Name")
			}, bReplace);
		},
		
		_onMasterMatched: function() {
			this.getOwnerComponent().oListSelector.oWhenListLoadingIsDone.then(
				function(mParams) {
					if (mParams.list.getMode() === "None") {
						return;
					}
					var sObjectId = mParams.firstListitem.getBindingContext().getProperty("Name");
					this.getRouter().navTo("object", {objectId: sObjectId}, true);
				}.bind(this),
				function(mParams) {
					if (mParams.error) {
						return;
					}
					this.getRouter().getTargets().dispay("detailNoObjectsAvailable");
				}.bind(this)
			);
		}
	});
});