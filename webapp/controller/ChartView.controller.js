sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/viz/ui5/data/FlattenedDataset",
	"sap/viz/ui5/controls/common/feeds/FeedItem",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/ui/model/Filter"
], function(Controller, JSONModel, FlattenedDataset, FeedItem, ODataModel, Filter) {
	"use strict";

	return Controller.extend("test.dore.dataChart.controller.ChartView", {
		
		onInit: function() {
			// 1. Get VizFrame by Id
			var oVizFrame = this.getView().byId("idcolumn");

			// 2. Create sample JSON data to dispay
			// var oModel = new JSONModel();
			// var data = {
			// 	"Cars": [{
			// 		"Model": "Alto",
			// 		"Value": "758620"
			// 	}, {
			// 		"Model": "Zen",
			// 		"Value": "431160"
			// 	}, {
			// 		"Model": "Santro",
			// 		"Value": "515100"
			// 	}, {
			// 		"Model": "Matiz",
			// 		"Value": "293780"
			// 	}, {
			// 		"Model": "Wagan R",
			// 		"Value": "974010"
			// 	}]
			// };
			// oModel.loadData("model/stats.json");
			var oModel = new ODataModel("/destinations/hanaStats", {
				disableHeadRequestForToken : true
			});
			// 3. Create Viz DataSet to fedd data to the graph
			var oFilter = new Filter("AS.Instance", "EQ", "sapdab7_AB7_07");
			var oFilter2 = new Filter("Time", "BT", "PT14H0M0S", "PT15H30M0S");
			var oDataset = new FlattenedDataset({
				dimensions: [{
					name: "Time",
					dataType: "date",
					value: {
						path: "Time",
						formatter: function(value) {
							var timeZoneOffset = new Date(0).getTimezoneOffset() * 60 * 1000;
							return new Date(value.ms + timeZoneOffset);
						}
					}
				}],
				measures: [{
					name: "CPU.Idle",
					value: "{CPU.Idle}"
				}, {
					name: "CPU.Sys",
					value: "{CPU.Sys}"
				}, {
					name: "CPU.Usr",
					value: "{CPU.Usr}"
				}]
			}).bindData("/monitoring", null, null, [oFilter, oFilter2]);
			oVizFrame.setDataset(oDataset);
			oVizFrame.setModel(oModel);
			oVizFrame.setVizType("timeseries_line");
			

			// 4. Setting VizFrame Poroperties
			oVizFrame.setVizProperties({
				legend: {
					isScrollable: true
				},
				plotArea: {
					window: {
						start: "firstDataPoint",
						end: "lastDataPoint"
					},
					colorPalette: d3.scale.category20().range()
				},
				timeAxis:{
					title:{"visible":true, "text":"Time"},
					levels:["second", "minute", "hour"],
					// levelConfig:{
					// 	second:{visible: false},
					// 	minute:{row: 1},
					// 	hour:{visible:false, row:1}
					// }
				}
			});
			var feedValueAxis = new FeedItem({
					uid: "valueAxis",
					type: "Measure",
					values: ["CPU.Idle"]
				}),
				feedValueAxis2 = new FeedItem({
					uid: "valueAxis",
					type: "Measure",
					values: ["CPU.Sys"]
				}),
				feedValueAxis3 = new FeedItem({
					uid: "valueAxis",
					type: "Measure",
					values: ["CPU.Usr"]
				}),
				feedCategoryAxis = new FeedItem({
					uid: "timeAxis",
					type: "Dimension",
					values: ["Time"]
				});
			oVizFrame.addFeed(feedValueAxis);
			oVizFrame.addFeed(feedValueAxis2);
			oVizFrame.addFeed(feedValueAxis3);
			oVizFrame.addFeed(feedCategoryAxis);
			// oVizFrame.setVizProperties({
			// 	plotArea: {
			// 		primaryScale: {
			// 			fixedRange: true,
			// 			maxValue: 900000,
			// 			minValue: 850000
			// 		}
			// 	}
			// });
		}
	});
});