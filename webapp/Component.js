sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"test/dore/dataChart/model/models",
	"test/dore/dataChart/controller/ListSelector"
], function(UIComponent, Device, models, ListSelector) {
	"use strict";

	return UIComponent.extend("test.dore.dataChart.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			
			this.oListSelector = new ListSelector();

			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);
			
			this.getRouter().initialize();      
		},
		
		destroy: function() {
			this.oListSelector.destroy();
			
			UIComponent.prototype.destroy.apply(this, arguments);
		},
		
		getContentDensityClass: function() {
			if (this._sContentDensityClass === undefined) {
				if (jQuery(document.body).hasClass("sapUiSizeCozy") || jQuery(document.body).hasClass("sapUiSizeCompact")) {
					this._sContentDensityClass = "";	
				} else if (!Device.support.touch) {
					this._sContentDensityClass = "sapUiSizeCompact";
				} else {
					this._sContentDensityClass = "sapUiSizeCozy";
				}
			}
			return this._sContentDensityClass;
		}
	});
});