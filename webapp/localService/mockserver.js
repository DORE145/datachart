sap.ui.define([
	"sap/ui/core/util/MockServer"
], function(MockServer) {
	"use strict";
		
	return {
		
		//initialize mockserver
		init: function() {
			//creation
			jQuery.sap.require("sap.ui.core.util.MockServer");
			var oMockServer = new MockServer({
				rootUri: "/destinations/hanaStats/"
			});
			
			//Configuration
			var oUriParameters = jQuery.sap.getUriParameters();
			MockServer.config({
				autoRespond: true,
				autoRespondAfter : oUriParameters.get("serverDelay") || 1000
			});
			
			//simulating metadata and mockdata
			var sPath = jQuery.sap.getModulePath("test.dore.dataChart.localService");
			oMockServer.simulate(sPath + "/metadata.xml", {
				sMockdataBaseUrl: sPath + "/mockdata",
				bGenerateMissingMockData: false
			});
			
			//start
			oMockServer.start();
			jQuery.sap.log.info("Running app with mock data");
		}
	};
});